<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="asset/bootstrap/css/bootstrap.min.css">
    <script src="asset/bootstrap/jquery.min.js"></script>
    <script src="asset/bootstrap/js/bootstrap.min.js"></script>

</head>
<body>


<div class="container">

    <form action="carousel.php" method="post" enctype="multipart/form-data">
        <div class="file">
            Select image to Upload:
            <input type="file" class="form-control-file" id="fileToUpload"  name="fileToUploaded">
        </div>
        <br>
        <br>


        <div class="form-group">
            <textarea class="form-control"  rows="3" name="Description" placeholder="Enter Your text here"></textarea>
        </div>
        <br>
        <br>

        <div class="form-group">
            <p>Date: <input type="date" name="date"></p>
        </div>

        <br>
        <br>

    <button type="submit" class="btn btn-default">Send</button>

    </form>


</div>

</body>
</html>